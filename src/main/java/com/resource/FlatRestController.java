package com.resource;

import com.entity.Flat;
import com.service.FlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mariana on 16.01.17.
 */
@RestController
@RequestMapping(value = "/api")
public class FlatRestController {

    @Autowired
    private FlatService flatService;

    @RequestMapping(value="/index", method = RequestMethod.GET)
    public List<Flat> addFlats(){

        Flat flat1 = Flat.builder().name("flat1").build();
        Flat flat2 = Flat.builder().name("flat2").build();
        Flat flat3 = Flat.builder().name("flat3").build();
        Flat flat4 = Flat.builder().name("flat4").build();

        flatService.save(flat1);
        flatService.save(flat2);
        flatService.save(flat3);
        flatService.save(flat4);

        List<Flat> flats= new ArrayList<>();
        flats.add(flat1);
        flats.add(flat2);
        flats.add(flat3);
        flats.add(flat4);

        return flats;

    }



}
