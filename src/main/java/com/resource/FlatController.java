package com.resource;

import com.service.FlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class FlatController {

    @Autowired
    private FlatService flatService;


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String printAllFlats(final ModelMap model){

        model.addAttribute("flats", flatService.findAll());

        return "home";
    }
}
