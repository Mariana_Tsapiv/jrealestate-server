package com.service.impl;

import com.entity.Flat;
import com.repository.FlatRepository;
import com.service.FlatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mariana on 16.01.17.
 */
@Service
public class FlatServiceImpl implements FlatService {

    @Autowired
    private FlatRepository flatRepository;


    @Override
    public void save(Flat flat) {
        flatRepository.save(flat);
    }

    @Override
    public List<Flat> findAll() {
        return flatRepository.findAll();
    }

    @Override
    public void saveAll(List<Flat> flats) {

    }
}
