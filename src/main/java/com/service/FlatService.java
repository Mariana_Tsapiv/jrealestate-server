package com.service;

import com.entity.Flat;

import java.util.List;

/**
 * Created by mariana on 16.01.17.
 */
public interface FlatService {

    void save(Flat flat);
    List<Flat> findAll();
    void saveAll(List<Flat> flats);

}
