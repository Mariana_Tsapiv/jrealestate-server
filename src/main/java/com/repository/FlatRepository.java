package com.repository;

import com.entity.Flat;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by mariana on 16.01.17.
 */
public interface FlatRepository extends MongoRepository<Flat, String> {


     Flat findByname(String name);
}
