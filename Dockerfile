FROM ubuntu

# Installing "add-apt-repository" support
RUN apt-get update && \
	apt-get install -y software-properties-common && \
	apt-get clean && \
rm -rf /var/lib/apt/lists


# Java
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
rm -rf /var/cache/oracle-jdk8-installer


RUN apt-get update && \
	apt-get install -y git-core && \
rm -rf /var/lib/apt/lists/*

RUN rm -rf spring-boot-mongo-docker-poc/
RUN git clone https://username:password/project.git
RUN cd spring-boot-mongo-docker-poc && \
mvn clean install